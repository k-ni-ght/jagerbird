# JagerBird #

JagerBird is an estate scoring application on Android. Easy to use, provides a quick feed back on places you consider to live.
Based on the (AndroidBootstrap template)[http://www.androidbootstrap.com/], the app is using the placeILive api as data provider.

---
### What is this repository for? ###

* Portfolio project, the most important is to have a clean app on the play app store !

### Who do I talk to? ###

* Owner/dev : <romain.rouge@gmail.com>

---
## Alpha version ##

### How do I get set up? ###

1. Download the source code.
1. Build with gradle (or run the wrapper).
1. (_OPTIONAL_) Add the signing key.
1. Install the apk.


### Application security ###

The keystore is currently located on my macbook, in an encrypted volume name 'confidential.dmg'.
The Jagerbird key is also there. All the sensitive data have to be stored on this volume.
