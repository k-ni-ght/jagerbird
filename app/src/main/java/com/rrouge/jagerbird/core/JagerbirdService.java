package com.rrouge.jagerbird.core;

import com.rrouge.jagerbird.core.parcelable.Audit;

import java.util.List;
import java.util.Locale;

import retrofit.RestAdapter;

/**
 * Jagerbird API service
 */
public class JagerbirdService {

    private RestAdapter restAdapter;

    /**
     * Create jagerbird service
     * Default CTOR
     */
    public JagerbirdService() {
    }

    /**
     * Create jagerbird service
     *
     * @param restAdapter The RestAdapter that allows HTTP Communication.
     */
    public JagerbirdService(RestAdapter restAdapter) {
        this.restAdapter = restAdapter;
    }

    private AuditService getAuditService() {
        return getRestAdapter().create(AuditService.class);
    }

    private RestAdapter getRestAdapter() {
        return restAdapter;
    }

    /**
     * Get the audit for a selected address from placeILive.com
     */
    public List<Audit> getAudits(String address) {
        return getAuditService().getAuditByAddress(address);
    }

    /**
     * Audits around the geo location.
     * Default radius is 100 meters.
     *
     * @param lat latitude
     * @param lng longitude
     * @return list of audits
     */
    public List<Audit> getAudits(double lat, double lng) {
        String param = String.format(Locale.getDefault(), "%1$,.6f,%2$,.6f", lat, lng);
        return getAuditService().getAuditByLocation(param);
    }

}