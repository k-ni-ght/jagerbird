package com.rrouge.jagerbird.core.parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Deserializer used by LocationLookupService.
 */
public class LocationLookupDeserializer implements JsonDeserializer<LatLng> {
    @Override
    public LatLng deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        double lat = json.getAsJsonObject().get("latitude").getAsDouble();
        double lng = json.getAsJsonObject().get("longitude").getAsDouble();
        return new LatLng(lat, lng);
    }
}
