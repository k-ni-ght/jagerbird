package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import java.util.List;

import auto.parcel.AutoParcel;

/**
 * Created by rrouge on 10/30/14.
 */
@AutoParcel
public abstract class Prediction implements Parcelable {
    public abstract String description();

    public abstract String id();

    public abstract String place_id();

    public abstract List<String> types();

    public static Prediction create(String description, String id, String place_id, List<String> types) {
        return new AutoParcel_Prediction(description, id, place_id, types);
    }
}
