package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import java.util.List;

import auto.parcel.AutoParcel;

/**
 * Created by rrouge on 10/30/14.
 */
@AutoParcel
public abstract class PlacePrediction implements Parcelable {
    public abstract String status();

    public abstract String error_message();

    public abstract List<Prediction> predictions();

    public static PlacePrediction create(String status, String error_message, List<Prediction> predictions) {
        return new AutoParcel_PlacePrediction(status, error_message, predictions);
    }


}

