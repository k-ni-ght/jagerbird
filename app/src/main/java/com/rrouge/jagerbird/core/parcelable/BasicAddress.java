package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

import auto.parcel.AutoParcel;

/**
 * Created by rrouge on 10/27/14.
 */
@AutoParcel
public abstract class BasicAddress implements Parcelable {

    public abstract String country();

    public abstract String city();

    public abstract String county();

    public abstract String street();

    public abstract String zip();

    public abstract double lat();

    public abstract double lng();

    public static BasicAddress create(String country, String city, String county,
                                      String street, String zip, double lat, double lng) {
        return new AutoParcel_BasicAddress(country, city, county, street, zip, lat, lng);
    }

    public String print() {
        return String.format("%s\n%s\n%s", county(), city(), street());
    }

    public LatLng location() {
        return new LatLng(lat(), lng());
    }
}
