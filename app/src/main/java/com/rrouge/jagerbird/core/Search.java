package com.rrouge.jagerbird.core;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.rrouge.jagerbird.util.Ln;

import org.joda.time.DateTime;

import java.io.Serializable;

/**
 * Search entity.
 * Model for database record.
 */
@SuppressWarnings("unused")
@Table(name = "Search")
public class Search extends Model implements Serializable {

    private static final long serialVersionUID = -6641292855569752036L;

    @Column(name = "Query")
    public String query;
    @Column(name = "Date")
    public long date;
    @Column(name = "Starred")
    public boolean isStarred;


    public Search() {
    }

    public Search(String query, long date, boolean isStarred) {
        this.query = query;
        this.date = date;
        this.isStarred = isStarred;
    }

    public void starIt() {
        save(this.query, true);
    }

    /**
     * Return the persisted search.
     * Updates the search if an entry with the same query exists.
     *
     * @param query     query for the place audit search.
     * @param isStarred star attribute.
     * @return persisted search.
     */
    public static Search save(String query, boolean isStarred) {
        String formattedQuery = query.trim();
        Search search = new Select()
                .from(Search.class)
                .where("Query = ? COLLATE NOCASE", formattedQuery)
                .executeSingle();

        if (search == null) {
            search = new Search(formattedQuery, DateTime.now().getMillis(), isStarred);
        } else {
            Ln.d("Search already exist. Updated.");
            search.date = DateTime.now().getMillis();
            search.isStarred = isStarred;
        }
        search.save();

        return search;
    }

    /**
     * Return the persisted search without starring it.
     * Updates the search if an entry with the same query exists.
     *
     * @param query query for the place audit search.
     * @return persisted search.
     */
    public static Search save(String query) {
        return save(query, false);
    }

    public String getFormattedQuery() {
        return query.replace("London", "");
    }
}
