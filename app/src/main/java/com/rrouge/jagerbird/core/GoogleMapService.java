package com.rrouge.jagerbird.core;

import com.rrouge.jagerbird.core.parcelable.PlacePrediction;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Services provided by the google apis.
 */
public interface GoogleMapService {

    @GET(Constants.Http.URL_AUTOCOMPLETE_FRAG)
    PlacePrediction autocomplete(@Query("input") String query,
                                 @Query("location") String location,
                                 @Query("radius") int radius,
                                 @Query("types") String types,
                                 @Query("components") String components,
                                 @Query("key") String apiKey);
}
