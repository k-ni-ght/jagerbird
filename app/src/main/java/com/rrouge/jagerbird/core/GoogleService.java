package com.rrouge.jagerbird.core;

import com.rrouge.jagerbird.core.parcelable.PlacePrediction;

import retrofit.RestAdapter;

/**
 * Google API service
 */
public class GoogleService {
    public static final String API_KEY = "AIzaSyB8EilkxnENiZruC_RlZB7QZp6b3DBQXkQ";
    public static final String LONDON_LOCATION = "51.507315,-0.127702";
    public static final int LONDON_RADIUS = 20000;
    public static final String TYPES = "address";
    public static final String UK_RESTRICTION = "country:uk";

    private RestAdapter restAdapter;

    /**
     * Create google service
     * Default CTOR
     */
    public GoogleService() {
    }

    /**
     * Create jagerbird service
     *
     * @param restAdapter The RestAdapter that allows HTTP Communication.
     */
    public GoogleService(RestAdapter restAdapter) {
        this.restAdapter = restAdapter;
    }

    private GoogleMapService getGoogleMapService() {
        return getRestAdapter().create(GoogleMapService.class);
    }

    private RestAdapter getRestAdapter() {
        return restAdapter;
    }

    /**
     * Get a list of places from an incomplete search string.
     */
    public PlacePrediction autocomplete(String searchString) {
        return getGoogleMapService().autocomplete(
                searchString,
                LONDON_LOCATION,
                LONDON_RADIUS,
                TYPES,
                UK_RESTRICTION,
                API_KEY
        );
    }

}
