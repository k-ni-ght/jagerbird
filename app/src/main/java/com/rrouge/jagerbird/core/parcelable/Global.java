package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import auto.parcel.AutoParcel;

/**
 * Created by rrouge on 10/26/14.
 */
@AutoParcel
public abstract class Global implements Parcelable {
    public abstract int value();

    public abstract String icon();

    public abstract String label();

    public static Global create(int value, String icon, String label) {
        return new AutoParcel_Global(value, icon, label);
    }

}