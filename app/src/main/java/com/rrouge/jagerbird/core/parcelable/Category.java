package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import auto.parcel.AutoParcel;

/**
 * Created by rrouge on 10/26/14.
 */
@AutoParcel
public abstract class Category implements Parcelable {
    public abstract String type();

    public abstract int value();

    public abstract String label();

    public abstract String icon();

    public static Category create(String type, int value, String label, String icon) {
        return new AutoParcel_Category(type, value, label, icon);
    }

    public enum LqiType {
        TRANSPORTATION("Transportation", "Transport"),
        DAILY_LIFE("Daily Life", "Daily Life"),
        SAFETY("Safety", "Safety"),
        HEALTH("Health", "Health"),
        SPORTS_AND_LEISURE("Sports And Leisure", "Sport/Leisure"),
        ENTERTAINMENT("Entertainment", "Entertainment"),
        DEMOGRAPHICS("Demographics", "Demographics"),
        NOT_FOUND("Category not found", "Not found");

        private final String label;
        private final String displayLabel;

        LqiType(String label, String displayLabel) {
            this.label = label;
            this.displayLabel = displayLabel;
        }

        public String label() {
            return label;
        }

        public String displayLabel() {
            return displayLabel;
        }

        static LqiType forName(String label) {
            LqiType catType = LqiType.NOT_FOUND;
            for (LqiType type : LqiType.values()) {
                if (type.label().equals(label)) {
                    catType = type;
                    break;
                }
            }

            return catType;
        }
    }

    public String typeShortLabel() {
        return LqiType.forName(type()).displayLabel();
    }
}