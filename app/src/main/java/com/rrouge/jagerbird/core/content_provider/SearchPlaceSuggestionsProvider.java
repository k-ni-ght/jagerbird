package com.rrouge.jagerbird.core.content_provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.rrouge.jagerbird.GoogleServiceProvider;
import com.rrouge.jagerbird.Injector;
import com.rrouge.jagerbird.core.parcelable.Prediction;
import com.rrouge.jagerbird.util.Ln;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

/**
 * Thread safe implementation.
 */
public class SearchPlaceSuggestionsProvider extends ContentProvider {

//    static final String TAG = "Jagerbird";

    static final String AUTHORITY = "content://com.rrouge.jagerbird.search_suggestion";
    public static final Uri CONTENT_URI = Uri.parse(AUTHORITY);
//    static final String SINGLE_RECORD_MIME_TYPE = "vnd.android.cursor.item/vnd.rrouge.jagerbird.status";

    static final String MULTIPLE_RECORDS_MIME_TYPE = "vnd.android.cursor.dir/vnd.rrouge.jagerbird.status";

    private static String lastUnyieldingQuery;
    private static final Map<String, Cursor> predictionsCache = Maps.newHashMap();

    @Inject GoogleServiceProvider mGoogleSp;

    @Override
    public boolean onCreate() {
        return true;
    }

    @Override
    public synchronized Cursor query(Uri uri,
                                     String[] projection,
                                     String selection,
                                     String[] selectionArgs,
                                     String sortOrder) {
        if (mGoogleSp == null) {
            Injector.inject(this);
        }

        Cursor cursor;
        if (lastUnyieldingQuery != null && selection.contains(lastUnyieldingQuery)) {
            /**
             * If we didn't have any result from last request,
             * and this one is more specific.
             */
            cursor = emptyCursor();

        } else {
            /**
             * First look at the cache.
             */
            cursor = predictionsCache.get(selection);

            if (cursor == null) {
                cursor = suggestions(selection);

                boolean isEmpty = cursor.isAfterLast();
                if (!isEmpty) {
                    predictionsCache.put(selection, cursor);
                } else {
                    lastUnyieldingQuery = selection;
                }
            }
        }


        return cursor;
    }

    private MatrixCursor emptyCursor() {
        String[] cols = new String[]{"_id", "index", "city", "street"};

        return new MatrixCursor(cols);
    }

    private Cursor suggestions(String query) {
        MatrixCursor cursor = emptyCursor();
        try {
            List<Prediction> predictions = suggestionsFromGoogle(query);
            fillCursor(cursor, predictions);
        } catch (IOException e) {
            Ln.e(e);
        }

        return cursor;
    }

    private void fillCursor(MatrixCursor cursor, List<Prediction> predictions) {
        int index = 0;
        for (Prediction prediction : predictions) {
            List<String> desc = Lists.newArrayList(Splitter.on(',')
                    .trimResults()
                    .omitEmptyStrings()
                    .split(prediction.description()));

            String city = desc.get(desc.size() - 2);
            String street = desc.get(0);

            if(city.contains("London")){
                cursor.addRow(new Object[]{prediction.id().hashCode(), index++, city, street});
            }
        }
    }

    private List<Prediction> suggestionsFromGoogle(String query) throws IOException {
        return mGoogleSp.getService()
                .autocomplete(query)
                .predictions();
    }

    @Override
    public String getType(Uri uri) {
        return MULTIPLE_RECORDS_MIME_TYPE;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED");
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED");
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        throw new UnsupportedOperationException("NOT IMPLEMENTED");
    }
}
