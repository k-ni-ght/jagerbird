package com.rrouge.jagerbird.core;

import com.rrouge.jagerbird.core.parcelable.Audit;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Placeilive api.
 * Search location audit.
 */
public interface AuditService {

    @GET(Constants.Http.URL_AUDIT_FRAG)
    List<Audit> getAuditByAddress(@Query("q") String query);

    @GET(Constants.Http.URL_AUDIT_FRAG)
    List<Audit> getAuditByLocation(@Query("ll") String coordinates);
}
