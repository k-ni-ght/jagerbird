package com.rrouge.jagerbird.core;

/**
 * Jagerbird constants
 */
public final class Constants {
    private Constants() {
    }

    public static final class Auth {
        private Auth() {
        }

        /**
         * Account type id
         */
        public static final String BOOTSTRAP_ACCOUNT_TYPE = "com.rrouge.jagerbird";

        /**
         * Account name
         */
        public static final String BOOTSTRAP_ACCOUNT_NAME = "jagerbird";

        /**
         * Provider id
         */
        public static final String BOOTSTRAP_PROVIDER_AUTHORITY = "com.rrouge.jagerbird.sync";

        /**
         * Auth token type
         */
        public static final String AUTHTOKEN_TYPE = BOOTSTRAP_ACCOUNT_TYPE;
    }

    /**
     * All HTTP is done through a REST style API built for demonstration purposes on Parse.com
     * Thanks to the nice people at Parse for creating such a nice system for us to use for jagerbird!
     */
    public static final class Http {
        private Http() {
        }


        /**
         * Base URL for all requests
         */
        public static final String URL_BASE = "http://api.placeilive.com";

        /**
         * Pil Audit URL
         */
        public static final String URL_AUDIT_FRAG = "/v1/houses/search";
        public static final String URL_AUDIT = URL_BASE + URL_AUDIT_FRAG;


        /**
         * Base URL for all requests to google apis.
         */
        public static final String URL_BASE_GOOGLE_API = "https://maps.googleapis.com";

        /**
         * Google Place URL. Adds the Json format parameter.
         */
        public static final String URL_AUTOCOMPLETE_FRAG = "/maps/api/place/autocomplete/json";
        public static final String URL_AUTOCOMPLETE = URL_BASE_GOOGLE_API + URL_AUTOCOMPLETE_FRAG;


        /**
         * ----------------------------------------------------------------
         */


        /**
         * Base URL for all requests to flynn geocode apis.
         */
        public static final String URL_BASE_FLYNN_API = "https://montanaflynn-geocoder.p.mashape.com";

        /**
         * Google Place URL. Adds the Json format parameter.
         */
        public static final String URL_FLYNN_FRAG = "/address";

        /**
         * ----------------------------------------------------------------
         */


        /**
         * Authentication URL
         */
        public static final String URL_AUTH_FRAG = "/1/login";
        public static final String URL_AUTH = URL_BASE + URL_AUTH_FRAG;

        /**
         * List Users URL
         */
        public static final String URL_USERS_FRAG = "/1/users";
        public static final String URL_USERS = URL_BASE + URL_USERS_FRAG;


        /**
         * List News URL
         */
        public static final String URL_NEWS_FRAG = "/1/classes/News";
        public static final String URL_NEWS = URL_BASE + URL_NEWS_FRAG;


        /**
         * List Checkin's URL
         */
        public static final String URL_CHECKINS_FRAG = "/1/classes/Locations";
        public static final String URL_CHECKINS = URL_BASE + URL_CHECKINS_FRAG;

        /**
         * PARAMS for auth
         */
        public static final String PARAM_USERNAME = "username";
        public static final String PARAM_PASSWORD = "password";


        public static final String PARSE_APP_ID = "zHb2bVia6kgilYRWWdmTiEJooYA17NnkBSUVsr4H";
        public static final String PARSE_REST_API_KEY = "N2kCY1T3t3Jfhf9zpJ5MCURn3b25UpACILhnf5u9";
        public static final String HEADER_PARSE_REST_API_KEY = "X-Parse-REST-API-Key";
        public static final String HEADER_PARSE_APP_ID = "X-Parse-Application-Id";
        public static final String CONTENT_TYPE_JSON = "application/json";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String SESSION_TOKEN = "sessionToken";


    }


    public static final class Extra {
        private Extra() {
        }

        public static final String AUDIT_ITEM = "audit_item";

        public static final String LOCATION = "location";

        public static final String QUERY = "query";

        public static final String OLD_QUERY = "old_query";

        public static final String BASIC_ADDRESS = "basic_address";

        public static final String USER = "user";

    }

    public static final class Intent {
        private Intent() {
        }

        /**
         * Action prefix for all intents created
         */
        public static final String INTENT_PREFIX = "com.rrouge.jagerbird.";

    }

    public static class Notification {
        private Notification() {
        }

        public static final int TIMER_NOTIFICATION_ID = 1000; // Why 1000? Why not? :)
    }

}


