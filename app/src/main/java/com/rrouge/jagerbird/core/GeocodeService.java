package com.rrouge.jagerbird.core;

import com.google.android.gms.maps.model.LatLng;

import retrofit.RestAdapter;

/**
 * Geocode API service
 */
public class GeocodeService {
    public static final String API_KEY = "YfvBCOCPaemshTkHFgWUgATsqDCFp18GQavjsnTVcZEQrvm3Hb";

    private RestAdapter restAdapter;

    /**
     * Create geocode service
     * Default CTOR
     */
    public GeocodeService() {
    }

    /**
     * Create geocode service
     *
     * @param restAdapter The RestAdapter that allows HTTP Communication.
     */
    public GeocodeService(RestAdapter restAdapter) {
        this.restAdapter = restAdapter;
    }

    private LocationLookupService getLocationLookupService() {
        return getRestAdapter().create(LocationLookupService.class);
    }

    private RestAdapter getRestAdapter() {
        return restAdapter;
    }

    /**
     * Get a list of places from an incomplete search string.
     */
    public LatLng coordinatesFromAddress(String address) {
        return getLocationLookupService().coordinatesFromAddress(API_KEY, address);
    }
}
