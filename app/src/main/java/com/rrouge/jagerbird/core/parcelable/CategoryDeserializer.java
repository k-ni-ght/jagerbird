package com.rrouge.jagerbird.core.parcelable;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by rrouge on 10/26/14.
 */
public class CategoryDeserializer implements JsonDeserializer<Category> {
    public Category deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        return context.deserialize(json, AutoParcel_Category.class);
    }
}
