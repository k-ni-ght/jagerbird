package com.rrouge.jagerbird.core.parcelable;

import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import auto.parcel.AutoParcel;

/**
 * Bo for 'place I live' query response.
 * <p/>
 * Created by rrouge on 10/22/14.
 */
@AutoParcel
public abstract class Audit implements Parcelable {
    enum AddressFields {
        COUNTRY, COUNTY, CITY, STREET, ZIP;
    }

    public abstract int id();

    abstract String address();

    public abstract String profileUrl();

    public abstract double lat();

    public abstract double lng();

    public abstract Global lqi();

    abstract Collection<Category> lqi_category();

    public static Audit create(int id, String address, String profileUrl, double lat, double lng, Global lqi, Collection<Category> lqi_category) {
        return new AutoParcel_Audit(id, address, profileUrl, lat, lng, lqi, lqi_category);
    }

    public Category category(Category.LqiType type) {
        String NOT_FOUND = Category.LqiType.NOT_FOUND.toString();
        Category result = Category.create(NOT_FOUND, 0, NOT_FOUND, NOT_FOUND);
        for (Category category : lqi_category()) {
            if (category.type().equals(type.label())) {
                result = category;
                break;
            }
        }

        return result;
    }

    public Global global() {
        return lqi();
    }

    public Collection<Category> categories() {
        return new ArrayList<>(lqi_category());
    }

    public BasicAddress pAddress() {
        Map<AddressFields, String> aFields = extractAddress(address(), profileUrl());
        return BasicAddress.create(
                aFields.get(AddressFields.COUNTRY),
                aFields.get(AddressFields.COUNTY),
                aFields.get(AddressFields.CITY),
                aFields.get(AddressFields.STREET),
                aFields.get(AddressFields.ZIP),
                lat(), lng()
        );
    }

    private Map<AddressFields, String> extractAddress(String address, String url) {
        Map<AddressFields, String> res = Maps.newHashMap();
        List<String> aFields = Lists.newArrayList(
                Splitter.on(',')
                        .trimResults()
                        .split(address)
        );

        List<String> uFields = Lists.newArrayList(
                Splitter.on('/')
                        .trimResults()
                        .split(url)
        );

        String street = uFields.get(uFields.size() - 1)
                .replace('_', ' ')
                .replace(aFields.get(1), "")
                .replaceFirst(aFields.get(0) + ".*", "")
                .trim();
        String county = uFields.get(uFields.size() - 2)
                .replace('_', ' ')
                .replaceFirst(aFields.get(1), "")
                .trim();

        res.put(AddressFields.COUNTRY, aFields.get(3));
        res.put(AddressFields.COUNTY, county);
        res.put(AddressFields.CITY, aFields.get(1));
        res.put(AddressFields.STREET, street);
        res.put(AddressFields.ZIP, aFields.get(0));

        return res;
    }

    public LatLng location() {
        return pAddress().location();
    }
}
