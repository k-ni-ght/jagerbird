package com.rrouge.jagerbird.core;

import com.google.android.gms.maps.model.LatLng;

import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Query;

/**
 * Rest api interface.
 */
public interface LocationLookupService {


    @GET(Constants.Http.URL_FLYNN_FRAG)
    LatLng coordinatesFromAddress(@Header("X-Mashape-Key") String apiKey,
                                  @Query("address") String query);
}
