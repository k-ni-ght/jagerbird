package com.rrouge.jagerbird;

import android.accounts.AccountManager;
import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rrouge.jagerbird.authenticator.ApiKeyProvider;
import com.rrouge.jagerbird.authenticator.JagerbirdAuthenticatorActivity;
import com.rrouge.jagerbird.authenticator.LogoutService;
import com.rrouge.jagerbird.core.Constants;
import com.rrouge.jagerbird.core.JagerbirdService;
import com.rrouge.jagerbird.core.PostFromAnyThreadBus;
import com.rrouge.jagerbird.core.RestAdapterRequestInterceptor;
import com.rrouge.jagerbird.core.RestErrorHandler;
import com.rrouge.jagerbird.core.UserAgentProvider;
import com.rrouge.jagerbird.core.content_provider.SearchPlaceSuggestionsProvider;
import com.rrouge.jagerbird.core.parcelable.Audit;
import com.rrouge.jagerbird.core.parcelable.AuditDeserializer;
import com.rrouge.jagerbird.core.parcelable.Category;
import com.rrouge.jagerbird.core.parcelable.CategoryDeserializer;
import com.rrouge.jagerbird.core.parcelable.Global;
import com.rrouge.jagerbird.core.parcelable.GlobalDeserializer;
import com.rrouge.jagerbird.core.parcelable.LocationLookupDeserializer;
import com.rrouge.jagerbird.core.parcelable.PlacePrediction;
import com.rrouge.jagerbird.core.parcelable.PlacePredictionDeserializer;
import com.rrouge.jagerbird.core.parcelable.Prediction;
import com.rrouge.jagerbird.core.parcelable.PredictionDeserializer;
import com.rrouge.jagerbird.ui.AuditActivity;
import com.rrouge.jagerbird.ui.AuditListFragment;
import com.rrouge.jagerbird.ui.MainActivity;
import com.rrouge.jagerbird.ui.SearchHistoryFragment;
import com.squareup.otto.Bus;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Dagger module for setting up provides statements.
 * Register all of your entry points below.
 */
@Module(
        complete = false,

        injects = {
                JagerbirdApplication.class,
                JagerbirdAuthenticatorActivity.class,
                SearchPlaceSuggestionsProvider.class,
                MainActivity.class,
                AuditActivity.class,
                AuditListFragment.class,
                SearchHistoryFragment.class,
        }
)
public class JagerbirdModule {

    @Singleton
    @Provides
    Bus provideOttoBus() {
        return new PostFromAnyThreadBus();
    }

    @Provides
    @Singleton
    LogoutService provideLogoutService(final Context context, final AccountManager accountManager) {
        return new LogoutService(context, accountManager);
    }

    @Provides
    JagerbirdService provideBootstrapService(@Named("pil_adapter") RestAdapter restAdapter) {
        return new JagerbirdService(restAdapter);
    }

    @Provides
    JagerbirdServiceProvider provideBootstrapServiceProvider(@Named("pil_adapter") RestAdapter restAdapter, ApiKeyProvider apiKeyProvider) {
        return new JagerbirdServiceProvider(restAdapter, apiKeyProvider);
    }

    @Provides
    GeocodeServiceProvider provideFlynnServiceProvider(@Named("flynn_adapter") RestAdapter restAdapter, ApiKeyProvider apiKeyProvider) {
        return new GeocodeServiceProvider(restAdapter, apiKeyProvider);
    }

    @Provides
    GoogleServiceProvider provideGoogleServiceProvider(@Named("google_adapter") RestAdapter restAdapter, ApiKeyProvider apiKeyProvider) {
        return new GoogleServiceProvider(restAdapter, apiKeyProvider);
    }

    @Provides
    ApiKeyProvider provideApiKeyProvider(AccountManager accountManager) {
        return new ApiKeyProvider(accountManager);
    }

    @Provides
    @Named("pil_converter")
    Gson providePilGson() {
        /**
         * GSON instance to use for all request  with date format set up for proper parsing.
         * <p/>
         * You can also configure GSON with different naming policies for your API.
         * Maybe your API is Rails API and all json values are lower case with an underscore,
         * like this "first_name" instead of "firstName".
         * You can configure GSON as such below.
         * <p/>
         *
         * public static final Gson GSON = new GsonBuilder().setDateFormat("yyyy-MM-dd")
         *         .setFieldNamingPolicy(LOWER_CASE_WITH_UNDERSCORES).create();
         */
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Audit.class, new AuditDeserializer())
                .registerTypeAdapter(Global.class, new GlobalDeserializer())
                .registerTypeAdapter(Category.class, new CategoryDeserializer())
                .setDateFormat("yyyy-MM-dd")
                .create();

        return gson;
    }

    @Provides
    @Named("google_converter")
    Gson provideGoogleGson() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(PlacePrediction.class, new PlacePredictionDeserializer())
                .registerTypeAdapter(Prediction.class, new PredictionDeserializer())
                .setDateFormat("yyyy-MM-dd")
                .create();

        return gson;
    }

    @Provides
    @Named("flynn_converter")
    Gson provideFlynnGson() {
        return new GsonBuilder()
                .registerTypeAdapter(LatLng.class, new LocationLookupDeserializer())
                .setDateFormat("yyyy-MM-dd")
                .create();
    }

    @Provides
    RestErrorHandler provideRestErrorHandler(Bus bus) {
        return new RestErrorHandler(bus);
    }

    @Provides
    RestAdapterRequestInterceptor provideRestAdapterRequestInterceptor(UserAgentProvider userAgentProvider) {
        return new RestAdapterRequestInterceptor(userAgentProvider);
    }

    @Provides
    @Named("pil_adapter")
    RestAdapter providePilRestAdapter(RestErrorHandler restErrorHandler,
                                      RestAdapterRequestInterceptor restRequestInterceptor,
                                      @Named("pil_converter") Gson gson) {
        return new RestAdapter.Builder()
                .setEndpoint(Constants.Http.URL_BASE)
                .setErrorHandler(restErrorHandler)
                .setRequestInterceptor(restRequestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build();
    }

    @Provides
    @Named("google_adapter")
    RestAdapter provideGoogleApiRestAdapter(RestErrorHandler restErrorHandler,
                                            RestAdapterRequestInterceptor restRequestInterceptor,
                                            @Named("google_converter") Gson gson) {
        return new RestAdapter.Builder()
                .setEndpoint(Constants.Http.URL_BASE_GOOGLE_API)
                .setErrorHandler(restErrorHandler)
                .setRequestInterceptor(restRequestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build();
    }

    @Provides
    @Named("flynn_adapter")
    RestAdapter provideFlynnApiRestAdapter(RestErrorHandler restErrorHandler,
                                           RestAdapterRequestInterceptor restRequestInterceptor,
                                           @Named("flynn_converter") Gson gson) {
        return new RestAdapter.Builder()
                .setEndpoint(Constants.Http.URL_BASE_FLYNN_API)
                .setErrorHandler(restErrorHandler)
                .setRequestInterceptor(restRequestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new GsonConverter(gson))
                .build();
    }
}
