package com.rrouge.jagerbird;

import dagger.Module;

/**
 * Add all the other modules to this one.
 */
@Module(
        includes = {
                AndroidModule.class,
                JagerbirdModule.class
        }
)
public class RootModule {
}
