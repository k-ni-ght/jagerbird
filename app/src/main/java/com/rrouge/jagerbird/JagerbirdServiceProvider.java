package com.rrouge.jagerbird;

import com.rrouge.jagerbird.authenticator.ApiKeyProvider;
import com.rrouge.jagerbird.core.JagerbirdService;

import retrofit.RestAdapter;

/**
 * Provider for a {@link com.rrouge.jagerbird.core.JagerbirdService} instance
 */
public class JagerbirdServiceProvider {

    private RestAdapter restAdapter;

    public JagerbirdServiceProvider(RestAdapter restAdapter, ApiKeyProvider keyProvider) {
        this.restAdapter = restAdapter;
    }

    /**
     * Gets service.
     * <p/>
     * This method blocks and shouldn't be called on the main thread.
     *
     * @return jagerbird service
     */
    public JagerbirdService getService() {
        return new JagerbirdService(restAdapter);
    }
}
