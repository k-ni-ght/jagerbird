package com.rrouge.jagerbird;

import com.rrouge.jagerbird.authenticator.ApiKeyProvider;
import com.rrouge.jagerbird.core.GoogleService;

import java.io.IOException;

import retrofit.RestAdapter;

/**
 * Created by rrouge on 10/30/14.
 */
public class GoogleServiceProvider {

    private RestAdapter restAdapter;
    private ApiKeyProvider keyProvider;

    public GoogleServiceProvider(RestAdapter restAdapter, ApiKeyProvider keyProvider) {
        this.restAdapter = restAdapter;
        this.keyProvider = keyProvider;
    }

    /**
     * Get service for configured key provider
     * <p/>
     * This method gets an auth key and so it blocks and shouldn't be called on the main thread.
     *
     * @return google service
     * @throws java.io.IOException
     * @throws android.accounts.AccountsException
     */
    public GoogleService getService()
            throws IOException {
        return new GoogleService(restAdapter);
    }
}
