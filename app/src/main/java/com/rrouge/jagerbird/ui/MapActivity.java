package com.rrouge.jagerbird.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.parcelable.BasicAddress;

import static com.rrouge.jagerbird.core.Constants.Extra.BASIC_ADDRESS;

public class MapActivity extends FragmentActivity {
    private GoogleMap mGmapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /**
         * Only start the activity if we have coordinates.
         */
        if (getIntent() != null && getIntent().getExtras() != null) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.map_activity);

            BasicAddress basicAddress = getIntent().getExtras().getParcelable(BASIC_ADDRESS);
            initMap(basicAddress);
        } else {
            finish();
        }

    }

    private void initMap(BasicAddress basicAddress) {
        mGmapFragment = ((SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map_fragment))
                .getMap();

        MarkerOptions marker = new MarkerOptions()
                .position(basicAddress.location())
                .title(basicAddress.print())
                .draggable(false);

        CameraUpdate cUpdate = CameraUpdateFactory
                .newLatLngZoom(basicAddress.location(), 14);

        mGmapFragment.addMarker(marker);
        mGmapFragment.animateCamera(cUpdate);
    }

}
