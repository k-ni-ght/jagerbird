package com.rrouge.jagerbird.ui;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.rrouge.jagerbird.Injector;
import com.rrouge.jagerbird.R;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

import static android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP;
import static android.content.Intent.FLAG_ACTIVITY_SINGLE_TOP;

/**
 * Base class for all Jagerbird Activities that need fragments.
 */
public class JagerBirdFragmentActivity extends ActionBarActivity {

    @Optional @InjectView(R.id.main_toolbar) protected Toolbar mToolbar;

    @Inject
    protected Bus eventBus;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Injector.inject(this);
    }

    @Override
    public void setContentView(final int layoutResId) {
        super.setContentView(layoutResId);

        ButterKnife.inject(this);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        eventBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        eventBus.unregister(this);
    }

    protected class OnClickNavBackFrom implements View.OnClickListener {
        private final Activity activity;

        OnClickNavBackFrom(Activity fromActivity) {
            this.activity = fromActivity;
        }

        @Override
        public void onClick(View v) {
            final Intent homeIntent = new Intent(activity, MainActivity.class);
            homeIntent.addFlags(FLAG_ACTIVITY_CLEAR_TOP | FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(homeIntent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);

        SearchView searchView = retrieveSearchView(searchItem);
        if (searchView != null) {
            initSearchView(searchView);
        }

        return super.onCreateOptionsMenu(menu);
    }

    private SearchView retrieveSearchView(MenuItem searchItem) {
        SearchView searchView = null;
        if (searchItem != null) {
//            searchView = (SearchView) searchItem.getActionView();
            searchView = new SearchView(this);
            MenuItemCompat.setShowAsAction(searchItem, MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
            MenuItemCompat.setActionView(searchItem, searchView);
            MenuItemCompat.expandActionView(searchItem);
        }
        return searchView;
    }

    private void initSearchView(SearchView searchView) {
        SearchManager searchManager = (SearchManager) JagerBirdFragmentActivity.this.getSystemService(Context.SEARCH_SERVICE);

        AutocompleteSearchBuilder searchBuilder = new AutocompleteSearchBuilder(searchView, eventBus);
        CursorAdapter adapter = searchBuilder.getNewAdapter();
        searchView.setSuggestionsAdapter(adapter);
        searchView.setPadding(0, 0, 72, 0);
        searchView.setOnSuggestionListener(searchBuilder.getNewSuggestionListener());
        searchView.setOnQueryTextListener(searchBuilder.getNewQueryListener(adapter));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(JagerBirdFragmentActivity.this.getComponentName()));
    }
}
