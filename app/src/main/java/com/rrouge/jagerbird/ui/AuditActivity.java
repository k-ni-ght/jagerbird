package com.rrouge.jagerbird.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.parcelable.Audit;

import butterknife.InjectView;

import static com.rrouge.jagerbird.core.Constants.Extra.AUDIT_ITEM;
import static com.rrouge.jagerbird.core.Constants.Extra.BASIC_ADDRESS;
import static com.rrouge.jagerbird.core.Constants.Extra.LOCATION;

public class AuditActivity extends JagerBirdFragmentActivity {

    private Audit mAudit;

    @InjectView(R.id.audit_toolbar) protected Toolbar mToolbar;
    @InjectView(R.id.audit_badge_container) protected GridView mBadgeContainer;
    @InjectView(R.id.adView) protected AdView mAdView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.audit_activity);

        String screenTitle = "Failed to load audit …";
        String screenSubTitle = "please try again";
        if (getIntent() != null && getIntent().getExtras() != null) {
            mAudit = getIntent().getExtras().getParcelable(AUDIT_ITEM);

            if (mAudit != null) {
                screenTitle = mAudit.pAddress().county();
                screenSubTitle = mAudit.pAddress().city();
                AuditCategoryAdapter adapter = new AuditCategoryAdapter(getLayoutInflater(), mAudit.categories());
                mBadgeContainer.setAdapter(adapter);

                setupAdmob();
            }

        }

        setupToolbar(screenTitle, screenSubTitle);
    }

    private void setupToolbar(String title, String subTitle) {
        mToolbar.setTitle(title);
        int titleColor = getResources().getColor(R.color.abc_secondary_text_material_dark);
        mToolbar.setTitleTextColor(titleColor);
        mToolbar.setSubtitle(subTitle);
        mToolbar.setNavigationIcon(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        mToolbar.setNavigationContentDescription("Return to main screen");
        mToolbar.setNavigationOnClickListener(new OnClickNavBackFrom(this));
        mToolbar.inflateMenu(R.menu.audit_activity);
        mToolbar.setOnMenuItemClickListener(new ToolBarClickListener(this));
    }

    private void setupAdmob() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("2D56C8EE06EAE2EE5ED2B8E5B963D025")
                .build();
        mAdView.loadAd(adRequest);
    }

    public void onPilLinkClick(View v) {
        Uri uriUrl = Uri.parse(String.format(mAudit.profileUrl()));
        Intent launchBrowser = new Intent(Intent.ACTION_VIEW, uriUrl);
        startActivity(launchBrowser);
    }

    private class ToolBarClickListener implements Toolbar.OnMenuItemClickListener {
        private final Activity activity;

        private ToolBarClickListener(Activity activity) {
            this.activity = activity;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            Intent intent = new Intent();
            final Bundle args = new Bundle();

            switch (menuItem.getItemId()) {
                case R.id.panorama_menu_item:
                    intent = new Intent(activity, PanoramaActivity.class);
                    args.putParcelable(LOCATION, mAudit.location());
                    break;

                case R.id.map_menu_item:
                    intent = new Intent(activity, MapActivity.class);
                    args.putParcelable(BASIC_ADDRESS, mAudit.pAddress());
                    break;
            }

            intent.putExtras(args);
            startActivity(intent);

            return false;
        }
    }
}
