package com.rrouge.jagerbird.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.parcelable.Audit;
import com.rrouge.jagerbird.ui.view.Indicator;
import com.rrouge.jagerbird.util.Ln;

import java.util.List;

public class AuditListAdapter extends ArrayAdapter<Audit> {

    private final Context mContext;
    private final List<Audit> auditList;

    public AuditListAdapter(Context context, List<Audit> items) {
        super(context, R.layout.audit_list_item, items);

        this.mContext = context;
        this.auditList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.audit_list_item, parent, false);

            holder = new ViewHolder();
            holder.lqiIndicator = (Indicator) v.findViewById(R.id.pb_lqi_indicator);
            holder.tv_location = (TextView) v.findViewById(R.id.tv_location);
            holder.tv_zip = (TextView) v.findViewById(R.id.tv_zip);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Audit audit = auditList.get(position);
        holder.inflate(audit);

        return v;
    }

    private static class ViewHolder {
        private Audit mAudit;

        Indicator lqiIndicator;
        TextView tv_location;
        TextView tv_zip;

        public void inflate(Audit audit) {
            if (audit.equals(mAudit)) {
                Ln.d("Not inflating list item");
            } else {
                mAudit = audit;

                int lqiValue = audit.lqi().value();

                lqiIndicator.setProgress(lqiValue);
                tv_location.setText(audit.pAddress().street());
                tv_zip.setText(String.format("%s - %s",
                        audit.pAddress().zip(),
                        audit.pAddress().county()));
            }
        }
    }
}