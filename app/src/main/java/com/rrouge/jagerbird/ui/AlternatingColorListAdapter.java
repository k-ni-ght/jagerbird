
package com.rrouge.jagerbird.ui;

import android.view.LayoutInflater;

import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.R.drawable;
import com.github.kevinsawicki.wishlist.SingleTypeAdapter;

import java.util.List;

/**
 * List adapter that colors rows in alternating colors
 *
 * @param <V>
 */
public abstract class AlternatingColorListAdapter<V> extends
        SingleTypeAdapter<V> {

    private final int primaryResource;

    private final int secondaryResource;

    /**
     * Create adapter with alternating row colors
     *
     * @param layoutId
     * @param inflater
     * @param items
     */
    public AlternatingColorListAdapter(final int layoutId, final LayoutInflater inflater,
                                       final List<V> items) {
        this(layoutId, inflater, items, true);
    }

    /**
     * Create adapter with alternating row colors
     *
     * @param layoutId
     * @param inflater
     * @param items
     * @param selectable
     */
    public AlternatingColorListAdapter(final int layoutId, final LayoutInflater inflater,
                                       final List<V> items, final boolean selectable) {
        super(inflater, layoutId);

        if (selectable) {
            primaryResource = drawable.abc_item_background_holo_light;
            secondaryResource = drawable.abc_item_background_holo_dark;
        } else {
            primaryResource = R.color.primary_dark_material_light;
            secondaryResource = R.color.primary_dark_material_dark;
        }

        setItems(items);
    }

    @Override
    protected void update(final int position, final V item) {
        if (position % 2 != 0)
            updater.view.setBackgroundResource(primaryResource);
        else
            updater.view.setBackgroundResource(secondaryResource);
    }
}
