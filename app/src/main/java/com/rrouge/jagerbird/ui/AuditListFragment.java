package com.rrouge.jagerbird.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.base.Optional;
import com.google.common.collect.Lists;
import com.rrouge.jagerbird.GeocodeServiceProvider;
import com.rrouge.jagerbird.Injector;
import com.rrouge.jagerbird.JagerbirdServiceProvider;
import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.authenticator.LogoutService;
import com.rrouge.jagerbird.core.GeocodeService;
import com.rrouge.jagerbird.core.JagerbirdService;
import com.rrouge.jagerbird.core.Search;
import com.rrouge.jagerbird.core.parcelable.Audit;
import com.rrouge.jagerbird.events.SearchRequestEvent;
import com.rrouge.jagerbird.util.Ln;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import retrofit.RetrofitError;

import static com.rrouge.jagerbird.core.Constants.Extra.AUDIT_ITEM;
import static com.rrouge.jagerbird.core.Constants.Extra.OLD_QUERY;
import static com.rrouge.jagerbird.core.Constants.Extra.QUERY;

@SuppressWarnings("unused")
public class AuditListFragment extends ItemListFragment<Audit> {

    @Inject
    protected JagerbirdServiceProvider serviceProvider;
    @Inject
    protected LogoutService logoutService;

    @Inject GeocodeServiceProvider geocodeServiceProvider;

    @Inject Bus bus;

    @Subscribe
    public void onSearchRequest(SearchRequestEvent searchRequest) {
        getListAdapter().getWrappedAdapter().clear();
        setListShown(false, true);

        Bundle extras = new Bundle();
        extras.putString(QUERY, searchRequest.query());
        extras.putBoolean(OLD_QUERY, searchRequest.isFromOldSearch());
        getLoaderManager().restartLoader(0, extras, this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(R.string.no_result);
    }

    @Override
    protected void configureList(Activity activity, ListView listView) {
        super.configureList(activity, listView);

        listView.setFastScrollEnabled(true);
        listView.setDividerHeight(0);

//        getListAdapter()
//                .addHeader(activity.getLayoutInflater()
//                        .inflate(R.layout.audit_list_item_labels, null));
    }

    @Override
    protected LogoutService getLogoutService() {
        return logoutService;
    }

    @Override
    public void onDestroyView() {
        setListAdapter(null);

        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public Loader<List<Audit>> onCreateLoader(int id, final Bundle args) {
        return new ThrowableLoader<List<Audit>>(getActivity(), items) {

            @Override
            public List<Audit> loadData() throws Exception {
                List<Audit> auditList = Lists.newArrayList();
                JagerbirdService appService = serviceProvider.getService();
                GeocodeService geoService = geocodeServiceProvider.getService();

                items.clear();

                Optional<String> query = getRawQuery(args);
                Boolean isSearchUsingOldQuery = args != null ? args.getBoolean(OLD_QUERY) : null;
                isSearchUsingOldQuery = isSearchUsingOldQuery != null ? isSearchUsingOldQuery : Boolean.TRUE;
                if (query.isPresent()) {
                    auditList = performSearch(query.get(), appService, geoService);

                    if (!(isSearchUsingOldQuery || auditList.isEmpty())) {
                        Search.save(query.get());
                    }
                }

                return auditList;
            }

            private Optional<String> getRawQuery(Bundle args) {
                Optional<String> query = Optional.absent();
                if (args != null) {
                    query = Optional.of(args.getString(QUERY));
                } else {
                    Search lastSearch = new Select().from(Search.class).orderBy("Date DESC").executeSingle();
                    if (lastSearch != null) {
                        query = Optional.of(lastSearch.query);
                    }
                }

                return query;
            }

            private List<Audit> performSearch(String query, JagerbirdService appService, GeocodeService
                    geoService) {
                List<Audit> resultList = Lists.newArrayList();

                LatLng latLng = geoService.coordinatesFromAddress(query);

                try {
                    resultList.addAll(appService.getAudits(latLng.latitude, latLng.longitude));
                } catch (RetrofitError error) {
                    Ln.d(error);
                }

                Collections.sort(resultList, new AuditComparatorByStreetName());

                return resultList;
            }

            class AuditComparatorByStreetName implements java.util.Comparator<Audit> {
                @Override
                public int compare(Audit lhs, Audit rhs) {
                    return lhs.pAddress().street().compareTo(rhs.pAddress().street());
                }
            }
        };
    }

    @Override
    protected ArrayAdapter<Audit> createAdapter(List<Audit> audits) {
        return new AuditListAdapter(getActivity(), audits);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        Audit audit = ((Audit) l.getItemAtPosition(position));

        startActivity(new Intent(getActivity(), AuditActivity.class).putExtra(AUDIT_ITEM, audit));
    }

    @Override
    protected int getErrorMessage(Exception exception) {
        return R.string.error_loading_audit;
    }

}
