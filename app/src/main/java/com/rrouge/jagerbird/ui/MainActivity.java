package com.rrouge.jagerbird.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.util.UIUtils;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Initial activity for the application.
 * <p/>
 * If you need to remove the authentication from the application please see
 * {@link com.rrouge.jagerbird.authenticator.ApiKeyProvider#getAuthKey(android.app.Activity)}
 */
public class MainActivity extends JagerBirdFragmentActivity {

    private final Fragment mAuditsFragment = new AuditListFragment();
    private final Fragment mSearchesFragment = new SearchHistoryFragment();

    @InjectView(R.id.search_history) protected ImageButton mSearchHistory;
    @InjectView(R.id.logo_requirement_google) protected ImageView mGoogleLogo;

    public static final int TRANSITION = FragmentTransaction.TRANSIT_FRAGMENT_OPEN;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {

        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);

        if (isTablet()) {
            setContentView(R.layout.main_activity_tablet);
        } else {
            setContentView(R.layout.main_activity);
        }

        // View injection with Butterknife
        ButterKnife.inject(this);

        initScreen();
    }

    private boolean isTablet() {
        return UIUtils.isTablet(this);
    }

    private void initScreen() {
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(TRANSITION)
                .add(R.id.container, mAuditsFragment, mAuditsFragment.getTag())
                .commit();

        final MainActivity mainActivity = this;
        getSupportFragmentManager().addOnBackStackChangedListener(
                new FragmentManager.OnBackStackChangedListener() {
                    @Override
                    public void onBackStackChanged() {
                        int visibility;
                        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                            visibility = View.GONE;
                        } else {
                            visibility = View.VISIBLE;
                        }
                        mainActivity.setToolBarActionsVisibility(visibility);
                        mGoogleLogo.setVisibility(visibility);
                    }
                }
        );
    }

    public void onSearchHistoryClick(View v) {
        getSupportFragmentManager()
                .beginTransaction()
                .setTransition(TRANSITION)
                .hide(mAuditsFragment)
                .add(R.id.container, mSearchesFragment, mSearchesFragment.getTag())
                .addToBackStack(mSearchesFragment.getTag())
                .commit();
    }

    private void setToolBarActionsVisibility(int visibility) {
        mToolbar.setVisibility(visibility);
        mSearchHistory.setVisibility(visibility);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
