package com.rrouge.jagerbird.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.activeandroid.query.Select;
import com.rrouge.jagerbird.Injector;
import com.rrouge.jagerbird.JagerbirdServiceProvider;
import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.authenticator.LogoutService;
import com.rrouge.jagerbird.core.Search;
import com.rrouge.jagerbird.events.SearchRequestEvent;
import com.squareup.otto.Bus;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

public class SearchHistoryFragment extends ItemListFragment<Search> {

    @Inject protected JagerbirdServiceProvider serviceProvider;
    @Inject protected LogoutService logoutService;

    @Inject Bus bus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Injector.inject(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setEmptyText(R.string.no_result);
    }

    @Override
    protected void configureList(Activity activity, ListView listView) {
        super.configureList(activity, listView);

        listView.setFastScrollEnabled(true);
        listView.setDividerHeight(0);
    }

    @Override
    protected LogoutService getLogoutService() {
        return logoutService;
    }

    @Override
    public void onDestroyView() {
        setListAdapter(null);

        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
        bus.register(this);
        items.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        bus.unregister(this);
    }

    @Override
    public Loader<List<Search>> onCreateLoader(int id, Bundle args) {
        return new ThrowableLoader<List<Search>>(getActivity(), items) {

            @Override
            public List<Search> loadData() throws Exception {

                if (getActivity() != null) {
                    return new Select().from(Search.class).orderBy("Date DESC").execute();
                } else {
                    return Collections.emptyList();
                }

            }
        };
    }

    @Override
    protected ArrayAdapter<Search> createAdapter(List<Search> items) {
        return new SearchHistoryListAdapter(getActivity(), items);
    }

    public void onListItemClick(ListView l, View v, int position, long id) {
        Search search = ((Search) l.getItemAtPosition(position));
        bus.post(new SearchRequestEvent(search.query, true));
        getFragmentManager().popBackStack();
    }

    @Override
    protected int getErrorMessage(Exception exception) {
        return R.string.error_loading_audit;
    }
}
