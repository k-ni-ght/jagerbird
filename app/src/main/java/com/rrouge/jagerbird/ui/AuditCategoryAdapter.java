package com.rrouge.jagerbird.ui;

import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.LayoutInflater;

import com.github.kevinsawicki.wishlist.SingleTypeAdapter;
import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.parcelable.Category;

import java.util.Collection;

public class AuditCategoryAdapter extends SingleTypeAdapter<Category> {

    public AuditCategoryAdapter(final LayoutInflater inflater) {
        this(inflater, null);
    }

    public AuditCategoryAdapter(final LayoutInflater inflater, @Nullable final Collection<Category> items) {
        super(inflater, R.layout.audit_category_badge);

        setItems(items);
    }

    @Override
    public long getItemId(final int position) {
        final String id = getItem(position).type();
        return !TextUtils.isEmpty(id) ? id.hashCode() : super
                .getItemId(position);
    }

    @Override
    protected int[] getChildViewIds() {
        return new int[]{R.id.category_type, R.id.category_result};
    }

    @Override
    protected void update(final int position, final Category category) {
        setText(0, category.typeShortLabel());
        ((com.rrouge.jagerbird.ui.view.Indicator) view(1)).setProgress(category.value());
    }

    private SpannableString buildResultSpannableString(String cat_value) {
        SpannableString ss = new SpannableString(cat_value);
        ss.setSpan(new AbsoluteSizeSpan(42, true), 0, cat_value.length(), 0);
        return ss;
    }
}
