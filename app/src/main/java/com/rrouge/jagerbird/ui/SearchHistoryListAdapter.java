package com.rrouge.jagerbird.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.Search;
import com.rrouge.jagerbird.util.Ln;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.PeriodFormatterBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SearchHistoryListAdapter extends ArrayAdapter<Search> {
    private final Context mContext;
    private final List<Search> mSearchHistoryList;

    public SearchHistoryListAdapter(Context context, List<Search> items) {
        super(context, R.layout.search_history_list_item, items);

        this.mContext = context;
        this.mSearchHistoryList = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        ViewHolder holder;

        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.search_history_list_item, parent, false);

            holder = new ViewHolder(v, R.id.tv_query, R.id.tv_abs_date, R.id.tv_rel_date, R.id.ic_star);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Search audit = mSearchHistoryList.get(position);
        holder.inflate(audit);

        return v;
    }

    private static class ViewHolder {
        private Search mSearch;

        TextView tv_query, tv_abs_date, tv_rel_date;
        ImageView ic_star;

        private ViewHolder(View list, int tv_query, int tv_abs_date, int tv_rel_date, int ic_star) {
            this.tv_query = (TextView) list.findViewById(tv_query);
            this.tv_abs_date = (TextView) list.findViewById(tv_abs_date);
            this.tv_rel_date = (TextView) list.findViewById(tv_rel_date);
            this.ic_star = (ImageView) list.findViewById(ic_star);
        }

        public void inflate(Search search) {
            if (search.equals(mSearch)) {
                Ln.d("Not inflating list item");
            } else {
                mSearch = search;
                DateTime abs_time = new DateTime().withMillis(search.date);
                Duration rel_time = new Duration(abs_time, DateTime.now());

                tv_query.setText(search.getFormattedQuery());
                tv_abs_date.setText(printDate(abs_time));
                tv_rel_date.setText(printPeriod(rel_time.toPeriod()));
                ic_star.setVisibility(search.isStarred ? View.VISIBLE : View.INVISIBLE);
            }
        }

        private String printDate(DateTime time) {
            DateTimeFormatterBuilder fBuilder = new DateTimeFormatterBuilder();

            if (isToday(time.toDate())) {
                fBuilder.appendLiteral("today ")
                        .appendHourOfDay(1)
                        .appendLiteral(":")
                        .appendMinuteOfHour(2);
            } else if (isYesterday(time.toDate())) {
                fBuilder.appendLiteral("yesterday ")
                        .appendHourOfDay(1)
                        .appendLiteral(":")
                        .appendMinuteOfHour(2);
            } else {
                fBuilder.append(DateTimeFormat.forPattern("dd-MM-yyyy"));
            }

            return fBuilder.toFormatter().print(time);
        }

        private String printPeriod(Period period) {
            Period normPeriod = period.normalizedStandard(PeriodType.yearWeekDayTime());

            PeriodFormatterBuilder formatterBuilder = new PeriodFormatterBuilder();

            if (normPeriod.getYears() > 0) {
                formatterBuilder.appendYears().appendSuffix(" year", " years");
            } else if (normPeriod.getWeeks() > 0) {
                formatterBuilder.appendWeeks().appendSuffix(" week", " weeks");
            } else if (normPeriod.getDays() > 0) {
                formatterBuilder.appendDays().appendSuffix(" day", " days");
            } else if (normPeriod.getHours() > 0) {
                formatterBuilder.appendHours().appendSuffix(" hour", " hours");
            } else if (normPeriod.getMinutes() > 0) {
                formatterBuilder.appendMinutes().appendSuffix(" min");
            } else {
                return "now";
            }

            return formatterBuilder.toFormatter().print(normPeriod);
        }

        private boolean isToday(Date date) {
            Date now = DateTime.now().toDate();
            DateFormat fmt = SimpleDateFormat.getTimeInstance();
            return fmt.format(now).equals(fmt.format(date));
        }

        private boolean isYesterday(Date date) {
            Date yesterday = DateTime.now().minusDays(1).toDate();
            DateFormat fmt = SimpleDateFormat.getTimeInstance();
            return fmt.format(yesterday).equals(fmt.format(date));
        }
    }
}

