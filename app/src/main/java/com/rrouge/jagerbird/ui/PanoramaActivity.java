package com.rrouge.jagerbird.ui;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.model.LatLng;
import com.rrouge.jagerbird.R;

import static com.rrouge.jagerbird.core.Constants.Extra.LOCATION;

public class PanoramaActivity extends FragmentActivity {
    private StreetViewPanorama mSvpFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /**
         * Only start the activity if we have coordinates.
         */
        if (getIntent() != null && getIntent().getExtras() != null) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.panorama_activity);

            LatLng latLng = getIntent().getExtras().getParcelable(LOCATION);
            initStreetViewPanorama(latLng);
        } else {
            finish();
        }

    }

    private void initStreetViewPanorama(LatLng latLng) {
        mSvpFragment = ((SupportStreetViewPanoramaFragment)
                getSupportFragmentManager().findFragmentById(R.id.sv_panorama_fragment))
                .getStreetViewPanorama();

        mSvpFragment.setPosition(latLng);
    }

}
