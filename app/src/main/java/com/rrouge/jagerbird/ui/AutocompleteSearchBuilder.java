package com.rrouge.jagerbird.ui;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.rrouge.jagerbird.R;
import com.rrouge.jagerbird.core.content_provider.SearchPlaceSuggestionsProvider;
import com.rrouge.jagerbird.events.SearchRequestEvent;
import com.rrouge.jagerbird.util.SafeAsyncTask;
import com.squareup.otto.Bus;

/**
 * Class containing the logic for the search view in the main activity.
 * <p/>
 * Created by rrouge on 3/7/15.
 */
public class AutocompleteSearchBuilder {
    private static final int QUERY_LENGTH_THRESHOLD = 3;
    public static final int CI_CITY = 2;
    public static final int CI_STREET = 3;


    private final SearchView mSearchView;

    private final Bus bus;

    public AutocompleteSearchBuilder(SearchView searchView, Bus bus) {
        this.mSearchView = searchView;
        this.bus = bus;
    }

    public CursorAdapter getNewAdapter() {
        String[] from = new String[]{"street", "city"};
        int[] to = new int[]{R.id.tv_street, R.id.tv_city};
        return new SimpleCursorAdapter(
                getContext(),                                   // The application's Context object
                R.layout.suggestion_dropdown_item_2lines,       // A layout in XML for one row in the ListView
                null,                                           // The result from the query
                from,                                           // A string array of column names in the cursor
                to,                                             // An integer array of view IDs in the row layout
                0);
    }

    private class QueryListenerJagerbirdImpl implements SearchView.OnQueryTextListener {
        private final CursorAdapter adapter;

        private QueryListenerJagerbirdImpl(CursorAdapter adapter) {
            this.adapter = adapter;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {
            hideSoftKeyboard(mSearchView);

            mSearchView.setQuery("", false);
            mSearchView.clearFocus();
            bus.post(new SearchRequestEvent(query, false));

            return true;
        }

        @Override
        public boolean onQueryTextChange(final String query) {
            if (query.length() > QUERY_LENGTH_THRESHOLD) {
                new RefreshSuggestionsTask(query, adapter, mSearchView).execute();
            }

            return true;
        }

        private void hideSoftKeyboard(View view) {
            InputMethodManager imm = (InputMethodManager) getContext()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private class SuggestionListenerJagerbirdImpl implements SearchView.OnSuggestionListener {
        @Override
        public boolean onSuggestionSelect(int i) {
            return false;
        }

        @Override
        public boolean onSuggestionClick(int i) {
            Cursor cursor = mSearchView
                    .getSuggestionsAdapter()
                    .getCursor();

            String city = cursor.getString(CI_CITY);
            String street = cursor.getString(CI_STREET);
            String query = String.format("%s %s", street, city);

            mSearchView.setQuery(query, false);
            return true;
        }
    }

    public SearchView.OnQueryTextListener getNewQueryListener(CursorAdapter adapter) {
        return new QueryListenerJagerbirdImpl(adapter);
    }

    public SearchView.OnSuggestionListener getNewSuggestionListener() {
        return new SuggestionListenerJagerbirdImpl();
    }


    private Context getContext() {
        return mSearchView.getContext();
    }

    private class RefreshSuggestionsTask extends SafeAsyncTask<Cursor> {
        private final String query;
        private final CursorAdapter adapter;
        private final SearchView searchView;

        private RefreshSuggestionsTask(String query, CursorAdapter adapter, SearchView searchView) {
            this.query = query;
            this.adapter = adapter;
            this.searchView = searchView;
        }

        @Override
        public Cursor call() throws Exception {
            return getContext().getContentResolver().query(
                    SearchPlaceSuggestionsProvider.CONTENT_URI, null,
                    query, null, null);
        }

        @Override
        protected void onSuccess(Cursor data) throws Exception {
            adapter.swapCursor(data);
            searchView.setSuggestionsAdapter(adapter);
        }
    }
}
