package com.rrouge.jagerbird;

import android.app.Instrumentation;
import android.content.Context;

import com.activeandroid.app.Application;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import net.danlew.android.joda.JodaTimeAndroid;

/**
 * jagerbird application
 */
public class JagerbirdApplication extends Application {

    private static JagerbirdApplication instance;

    /**
     * Create main application
     */
    public JagerbirdApplication() {
    }

    /**
     * Create main application
     *
     * @param context
     */
    public JagerbirdApplication(final Context context) {
        this();
        attachBaseContext(context);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;

        // Perform injection
        Injector.init(getRootModule(), this);

        JodaTimeAndroid.init(this);

    }

    private Object getRootModule() {
        return new RootModule();
    }


    /**
     * Create main application
     *
     * @param instrumentation
     */
    public JagerbirdApplication(final Instrumentation instrumentation) {
        this();
        attachBaseContext(instrumentation.getTargetContext());
    }

    public static JagerbirdApplication getInstance() {
        return instance;
    }
}
