package com.rrouge.jagerbird;

import com.rrouge.jagerbird.authenticator.ApiKeyProvider;
import com.rrouge.jagerbird.core.GeocodeService;

import java.io.IOException;

import retrofit.RestAdapter;

/**
 * Provider for a {@link com.rrouge.jagerbird.core.GeocodeService} instance
 */
public class GeocodeServiceProvider {

    private RestAdapter restAdapter;

    public GeocodeServiceProvider(RestAdapter restAdapter, ApiKeyProvider keyProvider) {
        this.restAdapter = restAdapter;
    }

    /**
     * Gets geocodeService service.
     *
     * @return geocodeService service
     */
    public GeocodeService getService()
            throws IOException {
        return new GeocodeService(restAdapter);
    }
}
