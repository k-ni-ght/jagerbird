package com.rrouge.jagerbird.events;

public class SearchRequestEvent {
    private final String query;
    private final boolean isFromOldSearch;

    public SearchRequestEvent(String query, boolean isFromOldSearch) {
        this.query = query;
        this.isFromOldSearch = isFromOldSearch;
    }

    public String query() {
        return this.query;
    }

    public boolean isFromOldSearch() {
        return this.isFromOldSearch;
    }

}
